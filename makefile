LD_FLAGS = -static -lsfml-graphics -lsfml-window -lsfml-system
CC = g++
CC_FLAGS = -Wall -std=c++11
CPP_FILES = $(wildcard src/*.cpp)
OBJ_FILES = $(patsubst src/%.cpp, obj/%.o, $(CPP_FILES))
DEBUG_OBJ_FILES = $(patsubst src/%.cpp, obj/%.debug.o, $(CPP_FILES))
TEST_OBJ_FILES = $(patsubst src/%.cpp, obj/%.test.o, $(CPP_FILES))

ifeq ($(OS),Windows_NT)
	RM = del /F /S /Q
	EXT = .exe
else
	RM = rm -rf
endif

all: release

doc: src/*.cpp misc/doxygen-bootstrapped
	-mkdir doc

	doxygen doxyfile misc/doxygen-bootstrapped/example-site/header.html \
	misc/doxygen-bootstrapped/example-site/footer.html \
	misc/doxygen-bootstrapped/customdoxygen.css \
	misc/doxygen-bootstrapped/doxy-boot.js \
	misc/images/preview.png

	echo img{max-width:100%%;} >> doc\html\customdoxygen.css

misc/doxygen-bootstrapped: misc
	git clone https://github.com/Velron/doxygen-bootstrapped.git misc/doxygen-bootstrapped

	
release: bin/paperjump$(EXT)

debug: bin/paperjump_debug$(EXT)

test: bin/paperjump_test$(EXT)


bin/paperjump$(EXT): $(OBJ_FILES)
	$(CC) -o $@ $^ $(LD_FLAGS) -mwindows

bin/paperjump_debug$(EXT): $(DEBUG_OBJ_FILES)
	$(CC) -o $@ $^ $(LD_FLAGS)

bin/paperjump_test$(EXT): $(TEST_OBJ_FILES)
	$(CC) -o $@ $^ $(LD_FLAGS)

obj:
	-mkdir obj

misc:
	-mkdir misc

obj/%.o: src/%.cpp obj
	$(CC) $(CC_FLAGS) -c -o $@ $<

obj/%.debug.o: src/%.cpp obj
	$(CC) $(CC_FLAGS) -D PAPER_DEBUG -ggdb -c -o $@ $<

obj/%.test.o: src/%.cpp obj
	$(CC) $(CC_FLAGS) -D PAPER_TEST -c -o $@ $<

clean:
	$(RM) obj
