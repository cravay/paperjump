/* Paper Jump
 * 14.06.2016
 *
 * Nikola Dimovic
 * Michael Schertenleib
 */

#include <SFML/Graphics.hpp>
#include <iostream>
#include "game_object.hpp"

Game_Object::Game_Object(std::string path) {
	set_texture(path);
}

Game_Object::Game_Object() {

}

void Game_Object::draw(sf::RenderWindow *window) {
	window->draw(sprite);
}

void Game_Object::update() {

}

void Game_Object::set_texture(std::string path) {
    if(!texture.loadFromFile(path)) {
        std::cerr << "Failed to load texture!" << std::endl;
	}

	sprite.setTexture(texture);

	rect = sprite.getTextureRect();
}
