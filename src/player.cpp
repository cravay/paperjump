/* Paper Jump
 * 14.06.2016
 *
 * Nikola Dimovic
 * Michael Schertenleib
 */

#include <SFML/Graphics.hpp>
#include <iostream>
#include <string>
#include "game.hpp"
#include "level.hpp"
#include "player.hpp"

Player::Player() : Game_Object("walkanimation.png") {
	velocity = 2;
	ticks = 0;
	offset = 0;
	is_walking = false;
	jump_key_pressed = false;
	game = nullptr;

	sf::Vector2u size = sprite.getTexture()->getSize();
	size.x /= 18;

	hitbox = sf::Rect<float>(0, 0, size.x - (border[0] + border[2]),
		size.y - (border[1] + border[3]));

	texture_rect = sf::Rect<int>(0, 0, size.x, size.y);
	sprite.setTextureRect(texture_rect);

	sprite.setOrigin(border[0] + hitbox.width / 2,
		border[1] + hitbox.height / 2);

	sprite.setScale(-1, 1);
}

void Player::update() {
	is_walking = false;

	if(hitbox.top >= 600) {
		spawn();
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) ^
		sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
			hitbox.left -= 2;
			sprite.setScale(1, 1);

			if(hitbox.left < 0) {
				hitbox.left = 0;
			}

			for(unsigned int i = 0; i < game->level->hitboxes.size(); i ++) {
				if(hitbox.intersects(game->level->hitboxes[i])) {
					hitbox.left = game->level->hitboxes[i].left + game->level->hitboxes[i].width;

				}
			}
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
			hitbox.left += 2;
			sprite.setScale(-1, 1);

			if(hitbox.left + hitbox.width > game->level->rect.width) {
				hitbox.left = game->level->rect.width - hitbox.width;
			}

			for(unsigned int i = 0; i < game->level->hitboxes.size(); i ++) {
				if(hitbox.intersects(game->level->hitboxes[i])) {
					hitbox.left = game->level->hitboxes[i].left - hitbox.width;
				}
			}
		}

		is_walking = true;
	}

	if(velocity < 2) {
		velocity += 0.2;

		if(velocity > 2) {
			velocity = 2;
		}
	}

	hitbox.top += velocity;

	if(velocity > 0) {
		for(unsigned int i = 0; i < game->level->hitboxes.size(); i ++) {
			if(hitbox.intersects(game->level->hitboxes[i])) {
				hitbox.top = game->level->hitboxes[i].top - hitbox.height;

				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up) ||
				sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
					if(!jump_key_pressed) {
						velocity = -5;
					}
					jump_key_pressed = true;
				} else {
					jump_key_pressed = false;
				}
			}
		}
	} else if(velocity < 0) {
		for(unsigned int i = 0; i < game->level->hitboxes.size(); i ++) {
			if(hitbox.intersects(game->level->hitboxes[i])) {
				hitbox.top = game->level->hitboxes[i].top + game->level->hitboxes[i].height;
				velocity = 0;
			}
		}
	}

	if(is_walking) {
		if((++ticks) % 12 == 0) {
			offset = (offset + 1) % 18;
		}
	} else {
		ticks = 0;
		offset = 0;
	}

	texture_rect.left = texture_rect.width * offset;
	sprite.setTextureRect(texture_rect);

	if(hitbox.intersects(game->level->finish)) {
		game->load_next_level();
	}
}

void Player::spawn() {
		hitbox.left = game->level->spawn.left;
		hitbox.top = game->level->spawn.top - hitbox.height;
}

// TODO: draw hitboxes and discard images
// TODO: the hitboxes should probably drawn in the level class
void Player::draw(sf::RenderWindow *window) {
	sprite.setPosition( - border[0] + hitbox.left + sprite.getOrigin().x,
		- border[1] + sprite.getOrigin().y + hitbox.top);

	Game_Object::draw(window);

	#ifdef PAPER_DEBUG

	sf::RectangleShape rectangle;
	rectangle.setOutlineColor(sf::Color::Red);
	rectangle.setFillColor(sf::Color::Transparent);
	rectangle.setOutlineThickness(1);

	for(unsigned int i = 0; i < game->level->hitboxes.size(); i ++) {
		rectangle.setSize(sf::Vector2f(game->level->hitboxes[i].width - 2, game->level->hitboxes[i].height - 2));
		rectangle.setPosition(game->level->hitboxes[i].left + 1, game->level->hitboxes[i].top + 1);
		window->draw(rectangle);
	}

	rectangle.setSize(sf::Vector2f(hitbox.width - 2, hitbox.height - 2));
	rectangle.setPosition(sf::Vector2f(hitbox.left + 1, hitbox.top + 1));
	window->draw(rectangle);

	rectangle.setOutlineColor(sf::Color(0xFF, 0xA5, 0x00, 0xFF));
	rectangle.setSize(sf::Vector2f(hitbox.width - 2, hitbox.height - 2));
	rectangle.setPosition(sf::Vector2f(game->level->spawn.left + 1, game->level->spawn.top + 1 - hitbox.height));
	window->draw(rectangle);

	rectangle.setOutlineColor(sf::Color::Blue);
	rectangle.setSize(sf::Vector2f(game->level->finish.width - 2, game->level->finish.height - 2));
	rectangle.setPosition(sf::Vector2f(game->level->finish.left + 1, game->level->finish.top + 1));
	window->draw(rectangle);

	#endif
}
