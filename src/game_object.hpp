/* Paper Jump
 * 14.06.2016
 *
 * Nikola Dimovic
 * Michael Schertenleib
 */

#ifndef GAME_OBJECT_HPP
#define GAME_OBJECT_HPP

/**
 * Base Class for everything that's visible on the screen.
 */
class Game_Object {	
		public:
			/**
			 * The texture of the Game_Object.
			 * Currently not used for anything.
			 */
			sf::Texture texture;

			/**
			 * The sprite of the Game_Object.
			 */
			sf::Sprite sprite;

			/**
			 * The dimensions of the Game_Object.
			 */
			sf::Rect<int> rect;

			/**
			 * Constructor.
			 * Takes a path to an image as argument.
			 */
			Game_Object(std::string path);
			
			/**
			 * Constructor without arguments.
			 * The image has to be set later.
			 */
			Game_Object();

			/**
			 * Draws the Game_Object on the screen.
			 */
			void draw(sf::RenderWindow *window);

			/**
			 * Updates the Game_Object.
			 * Has to be implemented by the child classes.
			 */
			void update();

			/**
			 * Sets the texture to the image with the passed path.
			 */
			void set_texture(std::string path);
};

#endif
