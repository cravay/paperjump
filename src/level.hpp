/* Paper Jump
 * 14.06.2016
 *
 * Nikola Dimovic
 * Michael Schertenleib
 */

#ifndef LEVEL_HPP
#define LEVEL_HPP

#include "game_object.hpp"

/**
 * Saves everything that is part of a level.
 * The saved information is used by the game object.
 */
class Level : public Game_Object {
	public:
		/**
		 * Spawnpoint of the level.
		 * Height and width are ignored.
		 */
		sf::Rect<float> spawn;

		/**
		 * End of the level.
		 * When the player is in this area the game object loads a new level.
		 */
		sf::Rect<float> finish;
		
		/**
		 * A vector with all the hitboxes.
		 * The player can't pass these rectangles.
		 */
		std::vector<sf::Rect<float> > hitboxes;

		/**
		 * Constructor.
		 * Takes an open file as argument.
		 * A level looks like this (comments are not supported):
		 * ```
		 * <path> # background image
		 * <x>, <y> # spawnpoint
		 * <x>, <y>, <height>, <width> # first hitbox
		 * <x>, <y>, <height>, <width> # second hitbox
		 * <x>, <y>, <height>, <width> # third hitbox
		 * ...
		 * ```
		 */
		Level(FILE *file);
};

#endif
