/* Paper Jump
 * 14.06.2016
 *
 * Nikola Dimovic
 * Michael Schertenleib
 */

#ifndef MENU_HPP
#define MENU_HPP

/**
 * Menu Class.
 */
class Menu {
	public:
		/**
		 * Processes input events and updates the selected index.
		 */
		void update(sf::RenderWindow *window);

		/**
		 * Draws the menu.
		 */
		void draw(sf::RenderWindow *window);

		/**
		 * Font.
		 */
		sf::Font *font;

		/**
		 * Menu items.
		 */
		std::vector<sf::Text> items;

		/**
		 * White background for the selected item.
		 */
		sf::RectangleShape item_background;

		/**
		 * White background.
		 */
		sf::RectangleShape background;

		/**
		 * Index of the currently selected item.
		 */
		unsigned int selected_item = 0;

		/**
		 * True when the menu is visible.
		 */
		bool visible = false;

		/**
		 * Constructor.
		 * Takes a vector of strings and a font.
		 */
		Menu(std::vector<std::string> items, sf::Font *font);
};

#endif
