/* Paper Jump
 * 14.06.2016
 *
 * Nikola Dimovic
 * Michael Schertenleib
 */

#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "game_object.hpp"

class Game;
class Menu;

/**
 * Player Class.
 */
class Player : public Game_Object {
	public:
		/**
		 * A pointer to the game the player is part of.
		 * This is used to access the level.
		 */
		Game *game;

		/**
		 * The amount of pixels the player falls down per tick.
		 * Can be between -5 (after jumping) and 2.
		 */
		double velocity;

		/**
		 * Used to change the sprite offset every 12th game-tick.
		 */
		int ticks;

		/**
		 * Offset of the part of the sprite which if visible.
		 */
		int offset;

		/**
		 * The position of the sprites texture on the screen.
		 */
		sf::Rect<int> texture_rect;

		/**
		 * Stores the hitbox of the player.
		 * The size of the hitbox is calculated by subtracting 
		 * the border from one 16th of the image size.
		 */
		sf::Rect<float> hitbox;

		/**
		 * Pixels in the border are drawn but not part of the hitbox.
		 * The first element is the left border, the second one is the
		 * top border etc.
		 */
		int border[4] = {5, 3, 10, 0};

		/**
		 * True when the player is walking.
		 * Used for the animation.
		 */
		bool is_walking;

		/**
		 * Set to true if the up arrow key has been pressed in a frame.
		 */
		bool jump_key_pressed;

		/**
		 * Constructor.
		 */
		Player();

		/**
		 * Checks the pressed keys and updates the player position.
		 */
		void update();

		/**
		 * Puts the player to the spawn position defined in the level.
		 */
		void spawn();

		/**
		 * Draws the player on the specified window.
		 */
		void draw(sf::RenderWindow *window);
};

#endif
