/* Paper Jump
 * 14.06.2016
 *
 * Nikola Dimovic
 * Michael Schertenleib
 */

#ifndef PAPER_TEST
#include "game.hpp"

int main() {
	Game game;
	game.run();

    return EXIT_SUCCESS;
}

#endif
