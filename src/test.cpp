#ifdef PAPER_TEST

#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "game.hpp"
#include "level.hpp"

TEST_CASE("The Game class can be initiated") {
	Game game;
	game.init();
	
	SECTION("Test if the spritesheet has been loaded") {
		REQUIRE((game.player.rect.height && game.player.rect.width));
	}

	SECTION("Test if level file has been opened") {
		REQUIRE(game.level != NULL);
	}
	
	SECTION("Test if menu has been loaded") {
		REQUIRE(game.menu != nullptr);
	}

	SECTION("The hitboxes have been loaded") {
		REQUIRE(game.level->hitboxes.size() >= 0);
	}
}

#endif