/* Paper Jump
 * 14.06.2016
 *
 * Nikola Dimovic
 * Michael Schertenleib
 */

#include <SFML/Graphics.hpp>
#include <iostream>
#include <string>
#include <sstream>
#include <iomanip>
#include <vector>
#include <fstream>
#include <cstdio>
#include "player.hpp"
#include "level.hpp"
#include "menu.hpp"
#include "game.hpp"

Game::Game() {
	player.game = this;
}

void Game::init() {
	#ifndef PAPER_TEST

    // create the main window
    window.create(sf::VideoMode(800, 600), "Paper Jump", sf::Style::Titlebar | sf::Style::Close );

	#endif

	sf::Image icon;
	if (!icon.loadFromFile("logo.png")) {
		return;
	}
	window.setIcon(icon.getSize().x, icon.getSize().y, icon.getPixelsPtr());

	view.setSize(800, 600);


	// TODO: create level-vector
	file = fopen("levels.txt", "r");
	level = new Level(file);

	// load timer text
	if (!font.loadFromFile("C:\\Windows\\Fonts\\Arial.ttf")) {
		std::cerr << "Failed to load C:\\Windows\\Fonts\\Arial.ttf";
		return;
	}

	text.setFont(font);
	text.setCharacterSize(24);
	text.setString("Timer: ");
	text.setColor(sf::Color::Black);
	text.setStyle(sf::Text::Bold);
	text.setPosition(10, 10);

	// default view
	default_view = window.getView();

	std::vector<std::string> items = {"start", "highscore", "credits", "exit"};
	menu = new Menu(items, &font);

	player.spawn();
}

void Game::run() {
	init();

    while(window.isOpen()) {
		// update
		if(menu->visible) {
			menu->update(&window);

		} else {
			// process events
			sf::Event event;
			while(window.pollEvent(event)) {
				// close window -> exit
				if(event.type == sf::Event::Closed) {
					window.close();
				} else if(event.type == sf::Event::KeyPressed) {
					if(event.key.code == sf::Keyboard::Escape) {
						menu->visible = true;
					}
					#ifdef PAPER_DEBUG

					 else if(event.key.code == sf::Keyboard::R) {
						if(sf::Keyboard::isKeyPressed(sf::Keyboard::LControl)) {
							delete level;
							rewind(file);
							level = new Level(file);
						}

						player.spawn();
					} else if(event.key.code == sf::Keyboard::N) {
						load_next_level();
					}
					#endif
				}
			}

			player.update();

			// update timer
			score = timer.getElapsedTime().asSeconds();

			// update perspective
			if(player.hitbox.left + player.hitbox.width / 2 <= 400) {
				view.setCenter(400, 300);

			} else if(player.hitbox.left + player.hitbox.width / 2 >= level->rect.width - 400) {
				view.setCenter(level->rect.width - 400, 300);
			} else {
				view.setCenter(player.hitbox.left + player.hitbox.width / 2, 300);
			}
		}

		// draw
		window.clear(sf::Color::White);

		window.setView(view);
		level->draw(&window);
		player.draw(&window);

		window.setView(default_view);

		if(menu->visible) {
			menu->draw(&window);
		} else {
			std::ostringstream ss;
			ss << std::setprecision(2) << std::fixed << score;
			text.setString("Timer: " + ss.str());
			window.draw(text);
		}

		// update the window
		window.display();

		// sleep 2 milliseconds
		// FPS is 50
		sf::sleep(sf::seconds(0.02) - clock.restart());
    }
	
	delete level;
	delete menu;
 }

 void Game::load_next_level() {
	delete level;

	if(feof(file)) {
		fclose(file);

		// TODO: check if highscore
		std::ofstream file("highscore.txt", std::ofstream::out | std::ofstream::app);
		file << score << std::endl;
		file.close();

		exit(EXIT_SUCCESS);
	}

	level = new Level(file);
	player.spawn();
}
