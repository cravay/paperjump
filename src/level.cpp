/* Paper Jump
 * 14.06.2016
 *
 * Nikola Dimovic
 * Michael Schertenleib
 */

#include <iostream>
#include <cstdio>
#include <SFML/Graphics.hpp>
#include "level.hpp"

// TODO: use <iostream>
Level::Level(FILE *file) : Game_Object() {

	int top, left, width, height;
	char filename_texture[50];

	// filename
	fscanf(file, "%49s", filename_texture);
	set_texture(filename_texture);

	// spawn
	fscanf(file, "%i ,%i", &top, &left);
	spawn = sf::Rect<float>(top, left, 0, 0);

	// finish
	fscanf(file, "%i ,%i, %i, %i", &top, &left, &width, &height);
	finish = sf::Rect<float>(top, left, width, height);


	// hitboxes
	while(fscanf(file, "%i ,%i, %i, %i", &top, &left, &width, &height) == 4) {
		hitboxes.push_back(sf::Rect<float>(top, left, width, height));
	}
}
