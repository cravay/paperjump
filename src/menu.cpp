/* Paper Jump
 * 14.06.2016
 *
 * Nikola Dimovic
 * Michael Schertenleib
 */

#include <SFML/Graphics.hpp>
#include <string>
#include <vector>
#include <iostream>
#include "menu.hpp"

// TODO: submenus
Menu::Menu(std::vector<std::string> items, sf::Font *font) {
	for(unsigned int i = 0; i < items.size(); i ++) {
		sf::Text text;
		text.setFont(*font);
		text.setString(items[i]);
		text.setColor(sf::Color::Black);
		text.setCharacterSize(40);
		text.setPosition(10, 10 + 50 * i);

		this->items.push_back(text);
	}

	this->items[0].setColor(sf::Color::White);

	item_background.setFillColor(sf::Color::Black);
	item_background.setOutlineThickness(0);
	item_background.setPosition(0, 10);
	item_background.setSize(sf::Vector2f(800, 50));

	background.setFillColor(sf::Color::White);
	background.setOutlineThickness(0);
	background.setPosition(0, 0);
	background.setSize(sf::Vector2f(800, 20 + 50 * items.size()));

	this->font = font;
}

void Menu::update(sf::RenderWindow *window) {
	sf::Event event;

	while (window->pollEvent(event)) {
		if(event.type == sf::Event::Closed) {
			window->close();
		} else if(event.type == sf::Event::KeyPressed) {
			if(event.key.code == sf::Keyboard::Down) {
				if(selected_item < items.size() - 1) {
					items[selected_item].setColor(sf::Color::Black);
					selected_item ++;
					item_background.setPosition(0, 10 + 50 * selected_item);
					items[selected_item].setColor(sf::Color::White);
				}
			} else if(event.key.code == sf::Keyboard::Escape) {
				visible = false;

			} else if(event.key.code == sf::Keyboard::Up) {
				if(selected_item > 0) {
					items[selected_item].setColor(sf::Color::Black);
					selected_item --;
					item_background.setPosition(0, 10 + 50 * selected_item);
					items[selected_item].setColor(sf::Color::White);
				}
			} else if(event.key.code == sf::Keyboard::Return) {
				switch(selected_item) {
					case(0):
						visible = false;
						break;
					case(3):
						window->close();
						break;
				}
			}
		}
	}
}

void Menu::draw(sf::RenderWindow *window) {
	window->draw(background);
	window->draw(item_background);

	for(sf::Text item : items) {
		window->draw(item);
	}
}
