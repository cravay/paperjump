/* Paper Jump
 * 14.06.2016
 *
 * Nikola Dimovic
 * Michael Schertenleib
 */

#ifndef GAME_HPP
#define GAME_HPP

#include <SFML/Graphics.hpp>
#include "player.hpp"

class Level;
class Player;

/**
 * Game Class.
 */
class Game {
	public:
		/**
		 * Current level.
		 */
		Level *level;

		/**
		 * Level file.
		 */
		FILE *file;

		/**
		 * Player of the Game.
		 */
		Player player;

		/**
		 * Time passed since the game started in seconds.
		 */
		double score;

		/**
		 * Font.
		 */
		sf::Font font;

		/**
		 * Menu.
		 */
		Menu *menu;

		/**
		 * Window the game is running in.
		 */
		sf::RenderWindow window;

		/**
		 * Used to wait between the frames
		 */
		sf::Clock clock;

		/**
		 * Used to keep track of the score.
		 */
		sf::Clock timer;


		/**
		 * Initializes the game.
		 */
		void init();

		/**
		 * The view used for the timer.
		 */
		sf::View default_view;
		
		/**
		 * The view for the background.
		 */
		sf::View view;

		/**
		 * Text
		 */
		sf::Text text;

		/**
		 * Constructor.
		 */
		Game();

		 /**
		 * Starts the game.
		 * Runs until the player finishes or the player closes the game.
		 */
		void run();

		/**
		 * Loads the next level.
		 * Closes the game when there are no more levels.
		 */
		void load_next_level();
};

#endif